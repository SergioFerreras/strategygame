﻿
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class InfluenceMapGraph : MonoBehaviour
{
    public float weightCanBeHit = 1;
    public float weightEnemyKingClose = 1;
    public float weightAlliesClose = 1;
    public float weightKillableEnemyClose = 1;
    public float weightHitableEnemyClose = 1;
    public float weightEnemyClose = 1;
    public float weightAllyKingClose = 1;
    public float weightCanHitKing = 1;
    public float weightCanKillKing = 1;

    [SerializeField] private float kingInfluenceDecay = 1f;
    [SerializeField] private float alliesInfluenceDecay = 1f;
    [SerializeField] private float enemiesInfluenceDecay = 1f;

    [SerializeField] private float maxNeighbourRadius = 1.05f;
    [SerializeField] private float maxAlignedTolerance = 0.05f;
    [SerializeField] private Color[] debugColors = null;
    [SerializeField] private bool UseDebugMode=false;

    private Node[] nodes;
    private Collider2D[] colliderContainer;
    private int[] usedDebugColors;
    private bool canUseDebugMode = false;
    public bool StoredParents { get; private set; } = false;
    bool[] visited;
    int[] distance;
    int[] parent;

    float[] alliesInfluenceBuffer;
    float[] enemiesInfluenceBuffer;

    Unit auxUnit=null;
    Unit auxEnemy=null;

    public static InfluenceMapGraph Instance;


    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        //intialize debug colors
        usedDebugColors = new int[debugColors.Length];
        for (int i = 0; i < usedDebugColors.Length; i++)
            usedDebugColors[i] = 0;

        //initalize variables
        colliderContainer = new Collider2D[22];
        int nodeIndex = 0;
        nodes = new Node[transform.childCount];
        LayerMask tilesLayer = LayerMask.GetMask("Tile");

        //create nodes
        foreach (Transform tileTransform in transform)
        {
            Tile tile = tileTransform.GetComponent<Tile>();
            tileTransform.GetComponent<Tile>().node = new Node(tile.isClear(),tile);
        }

        //populate neighbours based on tile position
        foreach (Transform tileTransform in transform)
        {
            Tile tile = tileTransform.GetComponent<Tile>();
            nodes[nodeIndex] = tile.node;
            tile.node.index = nodeIndex;

            int len = Physics2D.OverlapCircleNonAlloc(tileTransform.position, maxNeighbourRadius, colliderContainer, tilesLayer);

            for (int colliderIndex = 0; colliderIndex < len; colliderIndex++)
            {
                if (colliderContainer[colliderIndex].gameObject != tile.gameObject)
                {
                    if (Mathf.Abs(colliderContainer[colliderIndex].transform.position.x - tileTransform.position.x) <= maxAlignedTolerance) //alligned in the x axis
                    {
                        if (colliderContainer[colliderIndex].transform.position.y > tileTransform.position.y)
                        {
                            if (tile.node.TopNode == null) 
                                tile.node.TopNode = colliderContainer[colliderIndex].GetComponent<Tile>().node;
                            else
                                Debug.LogError("Neigbours have been reassigned, map graph is probably faulty, check your maxNeighbourRadius and maxAlignedTolerance and check for duplicated tiles");
                        }
                        else
                        {
                            if (tile.node.BottomNode == null) 
                                tile.node.BottomNode = colliderContainer[colliderIndex].GetComponent<Tile>().node;
                            else
                                Debug.LogError("Neigbours have been reassigned, map graph is probably faulty, check your maxNeighbourRadius and maxAlignedTolerance and check for duplicated tiles");
                        }
                    }

                    else if (Mathf.Abs(colliderContainer[colliderIndex].transform.position.y - tileTransform.position.y) <= maxAlignedTolerance) //alligned in the y axis
                    {
                        if (colliderContainer[colliderIndex].transform.position.x > tileTransform.position.x)
                        {
                            if (tile.node.RightNode == null)
                                tile.node.RightNode = colliderContainer[colliderIndex].GetComponent<Tile>().node;
                            else
                                Debug.LogError("Neigbours have been reassigned, map graph is probably faulty, check your maxNeighbourRadius and maxAlignedTolerance and check for duplicated tiles");
                        }
                        else
                        {
                            if (tile.node.LeftNode == null)
                                tile.node.LeftNode = colliderContainer[colliderIndex].GetComponent<Tile>().node;
                            else
                                Debug.LogError("Neigbours have been reassigned, map graph is probably faulty, check your maxNeighbourRadius and maxAlignedTolerance and check for duplicated tiles");
                        }
                    }
                }
            }
            if (tile.node.TopNode == null && tile.node.BottomNode == null && tile.node.RightNode == null && tile.node.LeftNode == null)
                Debug.LogWarning("There is an inaccesible tile in the mapGraph");
            nodeIndex++;
        }
        canUseDebugMode = true;
    }

    private void OnDrawGizmos()
    {
        if (canUseDebugMode && UseDebugMode)
        {
            updateGraphWalkability();
            debugDraw();
        }
    }

    private void debugDraw()
    {
        //reset debug colors
        for (int i = 0; i < usedDebugColors.Length; i++)
            usedDebugColors[i] = 0;

        //bfs
        bool[] visited = new bool[nodes.Length];

        for(int i = 0; i<nodes.Length; i++)
        {
            visited[i] = false;
        }

        for (int i = 0; i < nodes.Length; i++)
        {
            if (!visited[i] && nodes[i].Walkable)
            {
                Gizmos.color = findDebugColor();
                visited[i] = true;
                Queue<int> auxQueue = new Queue<int>();
                auxQueue.Enqueue(i);

                while (auxQueue.Count > 0)
                {
                    Node n = nodes[auxQueue.Dequeue()];
                    foreach (Node neighbour in n.Neighbours)
                    {
                        if (neighbour != null && neighbour.Walkable)
                        {
                            if (!visited[neighbour.index])
                            {
                                visited[neighbour.index] = true;
                                auxQueue.Enqueue(neighbour.index);
                            }
                            //draw lines to all neighbours that are walkable, included already visited ones
                            Gizmos.DrawLine(n.Tile.transform.position, neighbour.Tile.transform.position);
                        }
                    }
                }
            }
        }

        Gizmos.color = Color.black * 0.44f;

        for (int i = 0; i < nodes.Length; i++)
        {
            //if (visited[i]) //draws only the walkable nodes
                debugDrawNode(nodes[i]);
        }
    }

    private Color findDebugColor()
    {
        int timesRepeated = 0;
        while (true)
        {
            for (int i = 0; i < debugColors.Length; i++)
                if (timesRepeated == usedDebugColors[i])
                {
                    usedDebugColors[i]++;
                    return debugColors[i];
                }
            timesRepeated++;
        }
    }

    private void debugDrawNode(Node n)
    {
        Gizmos.DrawCube(n.Tile.transform.position-Vector3.forward, new Vector3 (0.15f,0.15f,0.01f));

# if UNITY_EDITOR
        UnityEditor.Handles.Label(n.Tile.transform.position, System.Convert.ToString(Math.Round(n.ValueCanBeHit, 1)));
#endif
    }

    private void updateGraphWalkability()
    {
        foreach (Node n in nodes)
        {
            n.Walkable = n.Tile.isClear();
        }
    }

    private void clearInfluence()
    {
        foreach (Node n in nodes)
        {
            n.TotalInfluence = 0;
            n.ValueAlliesClose = 0;
            n.ValueAllyKingClose = 0;
            n.ValueCanBeHit = 0;
            n.ValueCanHitKing = 0;
            n.ValueCanKillKing = 0;
            n.ValueEnemiesClose = 0;
            n.ValueEnemyKingClose = 0;
            n.ValueHitableEnemies = 0;
            n.ValueKillableEnemies = 0;
        }
    }

    public void RunOnEachTileBFS(Action<Tile> actionToRun, Tile originTile, int range, bool jumpOverObstacles = false, bool ignoreObstacles = false) //RANGE -1 = infinity
    {
        StoredParents = true;
        updateGraphWalkability();

        //bfs
        if (visited == null)
            visited = new bool[nodes.Length];
        if (distance == null)
            distance = new int[nodes.Length];
        if (parent == null)
            parent = new int[nodes.Length];

        for (int i = 0; i < nodes.Length; i++)
        {
            visited[i] = false;
            distance[i] = -1;
            parent[i] = -1;
        }

        visited[originTile.node.index] = true;
        distance[originTile.node.index] = 0;
        Queue<int> auxQueue = new Queue<int>();
        auxQueue.Enqueue(originTile.node.index);

        while (auxQueue.Count > 0)
        {
            Node n = nodes[auxQueue.Dequeue()];

            if ((n.Walkable || ignoreObstacles) && actionToRun != null)
                actionToRun(n.Tile);

            foreach (Node neighbour in n.Neighbours)
            {
                if (neighbour != null && (distance[n.index] < range || range == -1) && !visited[neighbour.index])
                {
                    if (neighbour.Walkable || jumpOverObstacles)
                    {
                        visited[neighbour.index] = true;
                        distance[neighbour.index] = distance[n.index] + 1;
                        parent[neighbour.index] = n.index;
                        auxQueue.Enqueue(neighbour.index);
                    }
                }
            }
        }
    }
    
    public void FindPaths (Tile originTile, int range, bool jumpOverObstacles = false, bool ignoreObstacles = false)
    {
        RunOnEachTileBFS(null, originTile, range, jumpOverObstacles, ignoreObstacles);
    }

    public Stack<Vector3> getPathTo(Tile target, bool keepStoredParents = false)
    {
        if (StoredParents)
        {
            if (!keepStoredParents)
                StoredParents = false;

            Stack<Vector3> path = new Stack<Vector3>();
            int currentNode = target.node.index;
            while (currentNode >= 0)
            {
                path.Push(nodes[currentNode].Tile.transform.position);
                currentNode = parent[currentNode];
            }
            return path;
        }
        Debug.LogError("Run RunOnEachTileBFS or FindPaths before running getPathTo");
        return new Stack<Vector3>();
    }

    public void updateInfluenceValues(Unit originUnit)
    {
        clearInfluence();

        Unit[] allUnits = FindObjectsOfType<Unit>();
        foreach (Unit u in allUnits)
        {
            if (u.playerNumber == 1 && u.GetComponent<Village>() == null && !u.isTree)//enemies
            {
                //tile can be hit by enemy
                RunOnEachTileBFS(tileCanBeHit, u.getCurrentTile(), u.attackRadius + u.tileSpeed, u.isFlying);

                //can hit or kill
                auxUnit = originUnit;
                auxEnemy = u;
                RunOnEachTileBFS(tileCanHitEnemy, u.getCurrentTile(), originUnit.attackRadius);
                auxUnit = null;
                auxEnemy = null;

                //enemies close
                if (enemiesInfluenceBuffer == null)
                    enemiesInfluenceBuffer = new float[nodes.Length];
                for (int i = 0; i < enemiesInfluenceBuffer.Length; i++)
                    enemiesInfluenceBuffer[i] = 0f;

                enemiesInfluenceBuffer[u.getCurrentTile().node.index] = 1;

                RunOnEachTileBFS(spreadEnemiesInfluenceToTile, u.getCurrentTile(), -1, true, true);

                //enemy king
                if (u.isKing)
                {
                    //king close
                    u.getCurrentTile().node.ValueEnemyKingClose = 1;
                    RunOnEachTileBFS(spreadEnemyKingInfluenceToTile,u.getCurrentTile(),-1,true,true);

                    //can hit or kill
                    auxUnit = originUnit;
                    auxEnemy = u;
                    RunOnEachTileBFS(tileCanHitKing, u.getCurrentTile(), originUnit.attackRadius);
                    auxUnit = null;
                    auxEnemy = null;
                }
            }

            else if(u.playerNumber == 2 && u.GetComponent<Village>() == null && !u.isTree)
            {
                //alies
                if (alliesInfluenceBuffer == null)
                    alliesInfluenceBuffer = new float[nodes.Length];
                for (int i = 0; i < alliesInfluenceBuffer.Length; i++)
                    alliesInfluenceBuffer[i] = 0f;
                if (u != originUnit)
                {
                    alliesInfluenceBuffer[u.getCurrentTile().node.index] = 1;
                }

                RunOnEachTileBFS(spreadAlliesInfluenceToTile, u.getCurrentTile(), -1, true, true);

                //ally king
                if (u.isKing)
                {
                    u.getCurrentTile().node.ValueAllyKingClose = 1;
                    RunOnEachTileBFS(spreadAllyKingInfluenceToTile, u.getCurrentTile(), -1, true, true);
                }
            }
        }

        foreach (Node n in nodes)
        {
            n.TotalInfluence =
            n.ValueAlliesClose * weightAlliesClose +
            n.ValueAllyKingClose * weightAllyKingClose +
            n.ValueCanBeHit * weightCanBeHit +
            n.ValueCanHitKing * weightCanHitKing +
            n.ValueCanKillKing * weightCanKillKing +
            n.ValueEnemiesClose * weightEnemyClose +
            n.ValueEnemyKingClose * weightEnemyKingClose +
            n.ValueHitableEnemies * weightHitableEnemyClose +
            n.ValueKillableEnemies * weightKillableEnemyClose;
        }

    }

    private void tileCanBeHit(Tile tile)
    {
        tile.node.ValueCanBeHit += 1;
    }

    private void tileCanHitKing(Tile tile)
    {
        tile.node.ValueCanHitKing = 1;

        if (auxUnit.attackDamage > auxEnemy.health)
            tile.node.ValueCanKillKing = 1;
    }

    private void tileCanHitEnemy(Tile tile)
    {
        tile.node.ValueHitableEnemies = 1;

        if (auxUnit.attackDamage > auxEnemy.health)
            tile.node.ValueKillableEnemies = 1;
    }

    private void spreadEnemyKingInfluenceToTile(Tile tile)
    {
        if (parent[tile.node.index]!=-1)
            tile.node.ValueEnemyKingClose = nodes[parent[tile.node.index]].ValueEnemyKingClose * Mathf.Exp(-1 * kingInfluenceDecay);
    }

    private void spreadAllyKingInfluenceToTile(Tile tile)
    {
        if (parent[tile.node.index] != -1)
            tile.node.ValueAllyKingClose = nodes[parent[tile.node.index]].ValueAllyKingClose * Mathf.Exp(-1 * kingInfluenceDecay);
    }

    private void spreadAlliesInfluenceToTile(Tile tile)
    {
        if (parent[tile.node.index] != -1)
        {
            alliesInfluenceBuffer[tile.node.index] = alliesInfluenceBuffer[parent[tile.node.index]] * Mathf.Exp(-1 * alliesInfluenceDecay);
        }
        tile.node.ValueAlliesClose += alliesInfluenceBuffer[tile.node.index];
    }

    private void spreadEnemiesInfluenceToTile(Tile tile)
    {
        if (parent[tile.node.index] != -1)
        {
            enemiesInfluenceBuffer[tile.node.index] = enemiesInfluenceBuffer[parent[tile.node.index]] * Mathf.Exp(-1 * enemiesInfluenceDecay);
        }
        tile.node.ValueEnemiesClose += enemiesInfluenceBuffer[tile.node.index];
    }
}
