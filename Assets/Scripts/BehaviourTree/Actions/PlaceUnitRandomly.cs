﻿using System.Collections.Generic;
using UnityEngine;

public class PlaceUnitRandomly : BNode
{
    Tile[] tiles;
    List<Tile> creatableTiles = new List<Tile>();

    public override NodeState Evaluate()
    {
        tiles = FindObjectsOfType<Tile>();
        foreach (Tile tile in tiles)
        {
            if (tile.isCreatable)
            {
                creatableTiles.Add(tile);
            }
        }

        if(creatableTiles.Count > 0)
        {
            int rand = Random.Range(0, creatableTiles.Count);
            creatableTiles[rand].CreateUnit();
            return NodeState.SUCCESS;
        }

        return NodeState.FAIL;
    }

    public override void OnTreeEnded()
    {
        creatableTiles.Clear();
    }

}