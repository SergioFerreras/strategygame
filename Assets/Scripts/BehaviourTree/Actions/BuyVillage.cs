﻿using UnityEngine;

public class BuyVillage: BNode
{
    GM gm;
    CharacterCreation characterCreation;
    [SerializeField] Village village = null;

    private void Awake()
    {
        gm = FindObjectOfType<GM>();
        characterCreation = FindObjectOfType<CharacterCreation>();
    }

    public override NodeState Evaluate()
    {
        print("Should I buy a " + village.name + "?");
        if (village.cost > gm.player2Gold)
        {
            print("I don't have enough money...");
            return NodeState.FAIL;
        }
        print("Buying...");
        characterCreation.BuyVillage(village);
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
	
    }

}