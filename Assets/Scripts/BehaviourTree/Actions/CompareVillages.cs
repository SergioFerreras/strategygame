﻿using UnityEngine;

public class CompareVillages: BNode
{

    Unit[] units = new Unit[0];

    public override NodeState Evaluate()
    {
        print("Do I have less villages than my enemy?");
        units = FindObjectsOfType<Unit>();
        if (countAllieVillages() < countEnemyVillages())
        {
            print("Yes! I need more villages!");
            return NodeState.SUCCESS;
        }
        print("Nah");
        return NodeState.FAIL;
    }

    public override void OnTreeEnded()
    {
	
    }

    private int countAllieVillages() //From AI perspective
    {
        int counter = 0;
        foreach (Unit myUnit in units)
        {
            if (myUnit.GetComponent<Village>() != null && myUnit.playerNumber == 2)
            {
                counter++;
            }
        }
        return counter;
    }

    private int countEnemyVillages() //From AI perspective
    {
        int counter = 0;
        foreach (Unit myUnit in units)
        {
            if (myUnit.GetComponent<Village>() != null && myUnit.playerNumber == 1)
            {
                counter++;
            }
        }
        return counter;
    }

}