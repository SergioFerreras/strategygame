﻿using UnityEngine;

public class CheckKingDanger: BNode
{
    [SerializeField] float canBeHit = 0.6f;
    [SerializeField] float enemiesClose = 0.5f;
    [SerializeField] float dangerValue;
    [SerializeField] float dangerTriggerValue = 1f;

    Unit[] units = new Unit[0];
    Unit king;

    private void Awake()
    {
        findKing();
    }

    public override NodeState Evaluate()
    {
        print("Checking if king is in danger...");
        dangerValue = king.GetNode().ValueCanBeHit * canBeHit + king.GetNode().ValueEnemiesClose * enemiesClose;
        if (dangerValue > dangerTriggerValue) //King is in danger
        {
            print("King is in danger!");
            return NodeState.SUCCESS;
        }
        print("King is not in danger");
        return NodeState.FAIL;
    }

    public override void OnTreeEnded()
    {
	
    }


    private void findKing()
    {
        units = FindObjectsOfType<Unit>();
        foreach (Unit myUnit in units)
        {
            if (myUnit.isKing && myUnit.playerNumber == 2)
            {
                king = myUnit;
            }
        }
    }

}