﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyUnitBattle: BNode
{
    GM gm;
    CharacterCreation characterCreation;
    [SerializeField] Unit unit;
    Unit[] units = new Unit[0];
    Unit king;
    [SerializeField] bool cheapestUnit;

    private void Awake()
    {
        gm = FindObjectOfType<GM>();
        characterCreation = FindObjectOfType<CharacterCreation>();
    }

    public override NodeState Evaluate()
    {
        print("Should I buy a " + unit.name + "?");
        print("King danger level: " + king.GetNode().ValueCanBeHit);
        if (unit.GetComponent<Village>() != null && unit.GetComponent<Village>().cost > gm.player2Gold) //Can't buy village
        {
            print("It costs " + unit.GetComponent<Village>().cost + " and I have " + gm.player2Gold);
            print(unit.name + " cost too much!");
            return NodeState.FAIL;
        }
        else if (unit.cost > gm.player2Gold) //Can't buy unit
        {
            print(unit.name + "cost too much!");
            return NodeState.FAIL;
        }
        else if (king.GetNode().ValueCanBeHit > 0.6f) //King is in danger TEN EN CUENTA TAMBIÉN ENEMYCLOSE
        {
            print("My king is in danger!");
            if (!cheapestUnit)
            {
                print(unit.name + "is not enough cheap to buy at this situation!");
                return NodeState.FAIL;
            }
            print("Knights are the cheapest. Buy them!");
            characterCreation.BuyUnit(unit);
            return NodeState.SUCCESS;
        }
        print("I don't know if I should buy the " + unit.name + "...");
        if (Random.Range(0f, 2f) <= 1f) //Cara o cruz --> Compro esta unidad
        {
            print("Yeah I should");
            characterCreation.BuyUnit(unit);
            return NodeState.SUCCESS; 
        }
        print("No I shouldn't");
        return NodeState.FAIL;
    }

    public override void OnTreeEnded()
    {
	
    }
}