﻿using System.Collections.Generic;
using UnityEngine;

public class PlaceVillage: BNode
{
    Tile[] tiles;
    List<Tile> creatableTiles = new List<Tile>();
    List<Tile> distanceTiles = new List<Tile>();

    public override NodeState Evaluate()
    {
        Unit king = FindKing();
        tiles = FindObjectsOfType<Tile>();
        foreach (Tile tile in tiles)
        {
            if (tile.isCreatable)
            {
                creatableTiles.Add(tile);
            }
        }

        if (creatableTiles.Count > 0)
        {
            float maxX = 0;
            float maxY = 0;
            float minX = 10000f;
            float minY = 10000f;
            Tile kingTile = king.getCurrentTile();
            foreach (Tile tile in creatableTiles)
            {
                float distanceX = Mathf.Abs(tile.transform.position.x - kingTile.transform.position.x);
                if (distanceX > maxX)
                {
                    maxX = distanceX;
                }
                if(distanceX < minX)
                {
                    minX = distanceX;
                }
                float distanceY = Mathf.Abs(tile.transform.position.y - kingTile.transform.position.y);
                if (distanceY > maxY)
                {
                    maxY = distanceY;
                }
                if (distanceY < minY)
                {
                    minY = distanceY;
                }
            }
            if (maxX >= maxY)
            {
                foreach (Tile tile in creatableTiles)
                {
                    if (Mathf.Abs(tile.transform.position.x - kingTile.transform.position.x) == minX)
                    {
                        distanceTiles.Add(tile);
                    }
                }
                distanceTiles.Sort(new CompareTilesDistanceYToACertainTile(kingTile));
            }
            else
            {
                foreach (Tile tile in creatableTiles)
                {
                    if (Mathf.Abs(tile.transform.position.y - kingTile.transform.position.y) == minY)
                    {
                        distanceTiles.Add(tile);
                    }
                }
                distanceTiles.Sort(new CompareTilesDistanceXToACertainTile(kingTile));
            }
            distanceTiles[distanceTiles.Count-1].CreateVillage();
            return NodeState.SUCCESS;
        }
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
        creatableTiles.Clear();
        distanceTiles.Clear();
    }

    Unit FindKing()
    {
        Unit[] allUnits = FindObjectsOfType<Unit>();
        foreach (Unit unit in allUnits)
        {
            if (unit.isKing && unit.playerNumber == 2)
            {
                return unit;
            }
        }
        return null;
    }

}
