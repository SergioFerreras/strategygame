﻿using UnityEngine;

public class PrintGoodBye : BNode
{

    public override NodeState Evaluate()
    {
        print("GoodBye");
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {

    }

}