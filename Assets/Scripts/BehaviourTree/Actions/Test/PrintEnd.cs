﻿using UnityEngine;


public class PrintEnd : BNode
{
    public override NodeState Evaluate()
    {
        print("End");
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
    }
}