﻿using UnityEngine;


public class OddConditional : BNode
{
    public override NodeState Evaluate()
    {
        if (Mathf.RoundToInt(Time.deltaTime * 1000) % 2 == 0)
        {
            print("es par");
            return NodeState.SUCCESS;
        }
        return NodeState.FAIL;
    }

    public override void OnTreeEnded()
    {
        
    }
}