﻿using UnityEngine;

public class Afail: BNode
{
    public override NodeState Evaluate()
    {
        return NodeState.FAIL;
    }

    public override void OnTreeEnded()
    {
	
    }

}