﻿using UnityEngine;

/// <summary>
/// This is a class
/// </summary>
public class PrintOtherWay : BNode
{
    public override NodeState Evaluate()
    {
        print("OtherWay");
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {

    }
}