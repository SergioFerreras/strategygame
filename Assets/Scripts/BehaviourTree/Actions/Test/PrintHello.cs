﻿using UnityEngine;

public class PrintHello : BNode
{

    public override NodeState Evaluate()
    {
        print("hello");
        return NodeState.SUCCESS;

    }

    public override void OnTreeEnded()
    {

    }
}