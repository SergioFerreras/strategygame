﻿using UnityEngine;

public class runningTest : BNode
{
    float timer = 0f;
    public override NodeState Evaluate()
    {
        print("running");
        if (timer < 5f)
        {
            timer += Time.deltaTime;
            return NodeState.RUNNING;
        }
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
        timer = 0f;
    }

}