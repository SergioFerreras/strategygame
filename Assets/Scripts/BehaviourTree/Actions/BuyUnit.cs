﻿using UnityEngine;

public class BuyUnit: BNode
{
    GM gm;
    CharacterCreation characterCreation;
    [SerializeField] Unit unit = null;

    private void Awake()
    {
        gm = FindObjectOfType<GM>();
        characterCreation = FindObjectOfType<CharacterCreation>();
    }

    public override NodeState Evaluate()
    {
        print("Should I buy a " + unit.name + "?");
        if (unit.cost > gm.player2Gold)
        {
            print("I don't have enough money...");
            return NodeState.FAIL;
        }
        print("Buying...");
        characterCreation.BuyUnit(unit);
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
	
    }

}