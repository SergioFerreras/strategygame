﻿using System.Collections.Generic;
using UnityEngine;

public class PlaceArcher: BNode
{
    Tile[] tiles;
    List<Tile> creatableTiles = new List<Tile>();
    List<Tile> distanceTiles = new List<Tile>();

    public override NodeState Evaluate()
    {
        Unit king = FindKing();
        tiles = FindObjectsOfType<Tile>();
        foreach (Tile tile in tiles)
        {
            if (tile.isCreatable)
            {
                creatableTiles.Add(tile);
            }
        }

        if (creatableTiles.Count > 0)
        {
            float maxX = 0;
            float maxY = 0;
            Tile kingTile = king.getCurrentTile();
            foreach (Tile tile in creatableTiles)
            {
                float distanceX = Mathf.Abs(tile.transform.position.x - kingTile.transform.position.x);
                if (distanceX > maxX)
                {
                    maxX = distanceX;
                }
                float distanceY = Mathf.Abs(tile.transform.position.y - kingTile.transform.position.y);
                if (distanceY > maxY)
                {
                    maxY = distanceY;
                }
            }
            if (maxX >= maxY)
            {
                foreach (Tile tile in creatableTiles)
                {
                    if (Mathf.Abs(tile.transform.position.x - kingTile.transform.position.x) == maxX - 2)
                    {
                        distanceTiles.Add(tile);
                    }
                }
            }
            else
            {
                foreach (Tile tile in creatableTiles)
                {
                    if (Mathf.Abs(tile.transform.position.y - kingTile.transform.position.y) == maxY - 2)
                    {
                        distanceTiles.Add(tile);
                    }
                }
            }
            if(distanceTiles.Count > 0)
            {
                int rand = Random.Range(0, distanceTiles.Count);
                distanceTiles[rand].CreateUnit();
                return NodeState.SUCCESS;
            }
            else
            {
                if(maxX >= maxY)
                {
                    creatableTiles.Sort(new CompareTilesDistanceXToACertainTile(kingTile));
                }
                else
                {
                    creatableTiles.Sort(new CompareTilesDistanceYToACertainTile(kingTile));
                }
                int rand = Random.Range(0, creatableTiles.Count);
                creatableTiles[rand].CreateUnit();
                return NodeState.SUCCESS;
            }
        }
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
        creatableTiles.Clear();
        distanceTiles.Clear();
    }

    Unit FindKing()
    {
        Unit[] allUnits = FindObjectsOfType<Unit>();
        foreach (Unit unit in allUnits)
        {
            if (unit.isKing && unit.playerNumber == 2)
            {
                return unit;
            }
        }
        return null;
    }

}

public class CompareTilesDistanceXToACertainTile : IComparer<Tile>
{
    Tile initialTile;

    public CompareTilesDistanceXToACertainTile(Tile initialTile)
    {
        this.initialTile = initialTile;
    }

    public int Compare(Tile tile1, Tile tile2)
    {
        float distance1 = Mathf.Abs(initialTile.transform.position.x - tile1.transform.position.x);
        float distance2 = Mathf.Abs(initialTile.transform.position.x - tile2.transform.position.x);
        if (distance1 < distance2)
        {
            return -1;
        }
        else if (distance1 == distance2)
        {
            return 0;
        }
        return 1;
    }
}

public class CompareTilesDistanceYToACertainTile : IComparer<Tile>
{
    Tile initialTile;

    public CompareTilesDistanceYToACertainTile(Tile initialTile)
    {
        this.initialTile = initialTile;
    }

    public int Compare(Tile tile1, Tile tile2)
    {
        float distance1 = Mathf.Abs(initialTile.transform.position.y - tile1.transform.position.y);
        float distance2 = Mathf.Abs(initialTile.transform.position.y - tile2.transform.position.y);
        if (distance1 < distance2)
        {
            return -1;
        }
        else if (distance1 == distance2)
        {
            return 0;
        }
        return 1;
    }
}