﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUnit: BNode
{
    private bool isRunning = true;
    private bool firstTime = true;

    public override NodeState Evaluate()
    {
        if (firstTime)
        {
            StartCoroutine(makeMove(GetComponentInParent<Unit>()));
            firstTime = false;
        }
        if (isRunning)
        {
            return NodeState.RUNNING;
        }
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
        isRunning = true;
        firstTime = true;
    }

    private IEnumerator makeMove(Unit unit) //Decides the place to move the unit (it has to be a place where it's able to move)
    {
        List<Tile> walkable = unit.GetWalkableTiles();
        if(walkable.Count > 0)
        {
            Tile highestWeight = walkable[0];
            yield return new WaitForSeconds(0.66f); //For showing the highlights of the walkable tiles on screen

            foreach (Tile tile in walkable)
            {
                if (highestWeight.node.TotalInfluence < tile.node.TotalInfluence)
                {
                    highestWeight = tile;
                }
            }
            unit.Move(highestWeight);
            yield return new WaitForSeconds(1.5f);
        }
        isRunning = false;
    }
    

}