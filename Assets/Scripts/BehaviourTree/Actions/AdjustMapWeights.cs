﻿using UnityEngine;

public class AdjustMapWeights: BNode
{
    private InfluenceMapGraph influenceMap;

    [SerializeField] float weightCanBeHit = 0.75f; //if it can be reached by an enemy attack, zero if it can't (MAX 1 per enemy)
    [SerializeField] float weightEnemyKingClose = 1; //bigger when enemy king is closer (MAX 1) -ignores obstacles-
    [SerializeField] float weightAlliesClose = 0.5f; //bigger when allies are closer (MAX 1 per ally) -ignores obstacles-
    [SerializeField] float weightKillableEnemyClose = 0.85f; //1 if you can kill enemy in that node (MAX 1 per enemy)
    [SerializeField] float weightHitableEnemyClose = 0.5f; //1 if you can kill enemy in that node (MAX 1 per enemy)
    [SerializeField] float weightEnemyClose = 0.25f; //bigger when enemies are closer (MAX 1 per enemy) -ignores obstacles-
    [SerializeField] float weightAllyKingClose = 0.25f; //bigger when ally king is closer (MAX 1) -ignores obstacles-
    [SerializeField] float weightCanHitKing = 1; //1 if king can be hit from there 0 if not (1 or 0)
    [SerializeField] float weightCanKillKing = 1; //1 if king can be killed from there 0 if not (1 or 0)

    public override NodeState Evaluate()
    {
        influenceMap = InfluenceMapGraph.Instance;
        changeMapWeights();
        influenceMap.updateInfluenceValues(GetComponentInParent<Unit>());
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
	
    }

    private void changeMapWeights()
    {
        
        //WEIGHTS
        influenceMap.weightCanBeHit = weightCanBeHit; //if it can be reached by an enemy attack, zero if it can't (MAX 1 per enemy)
        influenceMap.weightEnemyKingClose = weightEnemyKingClose; //bigger when enemy king is closer (MAX 1) -ignores obstacles-
        influenceMap.weightAlliesClose = weightAlliesClose; //bigger when allies are closer (MAX 1 per ally) -ignores obstacles-
        influenceMap.weightKillableEnemyClose = weightKillableEnemyClose; //1 if you can kill enemy in that node (MAX 1 per enemy)
        influenceMap.weightHitableEnemyClose = weightHitableEnemyClose; //1 if you can kill enemy in that node (MAX 1 per enemy)
        influenceMap.weightEnemyClose = weightEnemyClose; //bigger when enemies are closer (MAX 1 per enemy) -ignores obstacles-
        influenceMap.weightAllyKingClose = weightAllyKingClose; //bigger when ally king is closer (MAX 1) -ignores obstacles-
        influenceMap.weightCanHitKing = weightCanHitKing; //1 if king can be hit from there 0 if not (1 or 0)
        influenceMap.weightCanKillKing = weightCanKillKing; //1 if king can be killed from there 0 if not (1 or 0)
        

    }

}