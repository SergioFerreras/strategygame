﻿using System.Collections.Generic;
using UnityEngine;

public class AdjustProbabilities: BNode
{
    [SerializeField] RandomSelector randomSelector = null;
    Unit[] allUnits;

    //Number of ally troops
    int allies;
    int aKnights;
    int aArchers;
    int aBats;
    int aVillages;

    //Number of enemy troops
    int enemies;
    int eKnights;
    int eArchers;
    int eBats;
    int eVillages;

    float knightProb = 0.1f;
    float archerProb = 0.1f;
    float batProb = 0.1f;
    float villageProb = 0.05f;

    public override NodeState Evaluate()
    {
        allUnits = FindObjectsOfType<Unit>();
        CountUnits();
        CalculateVillageProbability();
        CalculateArcherProbability();
        CalculateKnightProbability();
        CalculateBatProbability();
        float[] finalProbabilites = new float[] { knightProb, archerProb, batProb, villageProb };
        randomSelector.ChangeDraftProbabilities(finalProbabilites);
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
        ClearCounts();
        ClearProbabilities();
    }

    void CalculateVillageProbability()
    {
        if (aVillages < eVillages)
        {
            villageProb = 1000f;
        }
    }

    void CalculateArcherProbability()
    {
        archerProb = eKnights * 0.1f + 0.1f;
    }

    void CalculateKnightProbability()
    {
        GM gm = FindObjectOfType<GM>();
        int gold = gm.player2Gold;
        if(gold <= 100)
        {
            knightProb = 0.3f;
        }
    }

    void CalculateBatProbability()
    {
        batProb = (eArchers + eBats) * 0.1f + 0.1f;
    }

    void CountUnits()
    {
        foreach (var unit in allUnits)
        {
            if (unit.playerNumber == 2)
            {
                allies += 1;
                IdentifyAllyType(unit);
            }
            else if(unit.playerNumber == 1)
            {
                enemies += 1;
                IdentifyEnemyType(unit);
            }
        }
    }

    void IdentifyAllyType(Unit unit)
    {
        switch (unit.unitType)
        {
            case UnitType.KING:
                break;
            case UnitType.KNIGHT:
                aKnights += 1;
                break;
            case UnitType.ARCHER:
                aArchers += 1;
                break;
            case UnitType.BAT:
                aBats += 1;
                break;
            case UnitType.VILLAGE:
                aVillages += 1;
                break;
            default:
                break;
        }
    }

    void IdentifyEnemyType(Unit unit)
    {
        switch (unit.unitType)
        {
            case UnitType.KING:
                break;
            case UnitType.KNIGHT:
                eKnights += 1;
                break;
            case UnitType.ARCHER:
                eArchers += 1;
                break;
            case UnitType.BAT:
                eBats += 1;
                break;
            case UnitType.VILLAGE:
                eVillages += 1;
                break;
            default:
                break;
        }
    }

    void ClearCounts()
    {
        //Number of ally troops
        allies = 0;
        aKnights = 0;
        aArchers = 0;
        aBats = 0;
        aVillages = 0;

        //Number of enemy troops
        enemies = 0;
        eKnights = 0;
        eArchers = 0;
        eBats = 0;
        eVillages = 0;
    }

    void ClearProbabilities()
    {
        knightProb = 0.1f;
        archerProb = 0.1f;
        batProb = 0.1f;
        villageProb = 0.05f;
    }

}