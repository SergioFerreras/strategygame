﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUnit: BNode
{
    private bool isRunning = true;
    private bool firstTime = true;

    public override NodeState Evaluate()
    {
        if (firstTime)
        {
            StartCoroutine(makeAttack(GetComponentInParent<Unit>()));
            firstTime = false;
        }
        if (isRunning)
        {
            return NodeState.RUNNING;
        }
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {
        isRunning = true;
        firstTime = true;
    }

    private IEnumerator makeAttack(Unit unit) //Decides who will be attacked (if any)
    {
        List<Unit> unitsToAttack = unit.enemiesInRange;
        unitsToAttack.Sort(new CompareHealth());
        yield return new WaitForSeconds(0.2f);
        if (unitsToAttack.Count > 0)
        {
            foreach(Unit attackUnit in unitsToAttack) //Kill king --> Max prior
            {
                if(attackUnit.unitType == UnitType.KING)
                {
                    unit.Attack(attackUnit);
                    isRunning = false;
                    yield break;
                }
            }
            foreach(Unit attackUnit in unitsToAttack) //If you can kill --> Kill
            {
                if(attackUnit.health <= unit.attackDamage && !attackUnit.isTree)
                {
                    unit.Attack(attackUnit);
                    isRunning = false;
                    yield break;
                }
            }
            foreach (Unit attackUnit in unitsToAttack) //If you don't die --> attack
            {
                if (unit.health > attackUnit.defenseDamage && !attackUnit.isTree)
                {
                    unit.Attack(attackUnit);
                    isRunning = false;
                    yield break;
                }
            }
            foreach (Unit attackUnit in unitsToAttack) //Prior --> Attack unit before tree
            {
                if (!attackUnit.isTree)
                {
                    unit.Attack(attackUnit);
                    isRunning = false;
                    yield break;
                }
            }
            unit.Attack(unitsToAttack[0]);
        }
        isRunning = false;
    }

}

public class CompareHealth : IComparer<Unit>
{
    public int Compare(Unit x, Unit y)
    {
        if (x.health < y.health)
        {
            return -1;
        }
        else if (x.health == y.health)
        {
            return 0;
        }
        return 1;
    }
}