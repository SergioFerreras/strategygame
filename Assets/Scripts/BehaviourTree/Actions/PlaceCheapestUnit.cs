﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PlaceCheapestUnit: BNode
{
    InfluenceMapGraph influenceMap;
    List<Tile> tileList = new List<Tile>();
    Unit king;

    private void Awake()
    {  
        king = FindKing();
    }

    public override NodeState Evaluate()
    {
        influenceMap = InfluenceMapGraph.Instance;
        searchBFS();
        if(tileList.Count > 0)
        {
            tileList[0].CreateUnit();
        }
        return NodeState.SUCCESS;
    }

    public override void OnTreeEnded()
    {

    }

    private void searchBFS()
    {
        tileList.Clear();
        influenceMap.RunOnEachTileBFS(addTileToList, king.getCurrentTile(), -1, true, false);
    }

    private void addTileToList(Tile tile)
    {
        tileList.Add(tile);
    }

    Unit FindKing()
    {
        Unit[] allUnits = FindObjectsOfType<Unit>();
        foreach (Unit unit in allUnits)
        {
            if (unit.isKing && unit.playerNumber == 2)
            {
                return unit;
            }
        }
        return null;
    }

}