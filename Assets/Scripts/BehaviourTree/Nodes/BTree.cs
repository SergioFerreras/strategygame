﻿using UnityEngine;

public enum NodeState { FAIL = 0, RUNNING = 1, SUCCESS = 2 }

public class BTree : MonoBehaviour
{
    [SerializeField] BNode initialNode = null;
    BNode[] allNodes = new BNode[] { };

    private void Awake()
    {
        allNodes = GetComponentsInChildren<BNode>();
    }

    public bool EvaluateTree()
    {
        NodeState state = initialNode.Evaluate();

        switch (state)
        {
            case NodeState.FAIL:
                resetNodes();
                return true;
            case NodeState.SUCCESS:
                resetNodes();
                return true;
            default:
                return false;
        }
    }

    void resetNodes()
    {
        foreach (BNode node in allNodes)
        {
            node.OnTreeEnded();
        }
    }
}