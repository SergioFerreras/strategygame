﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GamePhase { DRAFT, BATTLE}

public class GM : MonoBehaviour
{
    [HideInInspector] public Unit selectedUnit;

    [HideInInspector] public int playerTurn = 1;

    [HideInInspector] public Transform selectedUnitSquare;

    private Animator camAnim;
    [Header("The lower cost of a unit")]
    public float lowerCost = 40;
    [Header("Icons")]
    public Image playerIcon; 
    public Sprite playerOneIcon;
    public Sprite playerTwoIcon;
    [Header("Panel")]
    public GameObject unitInfoPanel;
    public Vector2 unitInfoPanelShift;
    Unit currentInfoUnit;
    [Header("PanelInfo")]
    public Text heathInfo;
    public Text attackDamageInfo;
    public Text armorInfo;
    public Text defenseDamageInfo;
    [Header("PlayersInitialGold")]
    public int player1Gold;
    public int player2Gold;
    [Header("GoldText")]
    public Text player1GoldText;
    public Text player2GoldText;

    [HideInInspector] public Unit createdUnit;
    [HideInInspector] public Village createdVillage;
    [Header("VictoryIcons")]
    public GameObject blueVictory;
    public GameObject darkVictory;

	private AudioSource source;

    [HideInInspector] public GamePhase gamePhase;
    private AI randomAI;

    private void Start()
    {
        randomAI = FindObjectOfType<AI>();
		source = GetComponent<AudioSource>();
        camAnim = Camera.main.GetComponent<Animator>();
        gamePhase = GamePhase.DRAFT;
        RandomizeTrees();
        UpdateGoldText();
    }

    private void Update()
    {
        if (gamePhase == GamePhase.DRAFT)
        {
            if (player1Gold < lowerCost && player2Gold < lowerCost && createdUnit == null && createdVillage == null)
            {
                StartBattlePhase();
            }
        }

        else if (playerTurn == 1 && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown("b")))
            EndTurn();


        if (selectedUnit != null) // moves the white square to the selected unit!
        {
            selectedUnitSquare.gameObject.SetActive(true);
            selectedUnitSquare.position = selectedUnit.transform.position;
        }
        else
        {
            selectedUnitSquare.gameObject.SetActive(false);
        }

    }

    void StartBattlePhase()
    {
        playerTurn = 2;
        gamePhase = GamePhase.BATTLE;
        EndTurn();
    }

    // Sets panel active/inactive and moves it to the correct place
    public void UpdateInfoPanel(Unit unit) {

        if (unit.Equals(currentInfoUnit) == false)
        {
            unitInfoPanel.transform.position = (Vector2)unit.transform.position + unitInfoPanelShift;
            unitInfoPanel.SetActive(true);

            currentInfoUnit = unit;

            UpdateInfoStats();

        } else {
            unitInfoPanel.SetActive(false);
            currentInfoUnit = null;
        }

    }

    // Updates the stats of the infoPanel
    public void UpdateInfoStats() {
        if (currentInfoUnit != null)
        {
            attackDamageInfo.text = currentInfoUnit.attackDamage.ToString();
            defenseDamageInfo.text = currentInfoUnit.defenseDamage.ToString();
            armorInfo.text = currentInfoUnit.armor.ToString();
            heathInfo.text = currentInfoUnit.health.ToString();
        }
    }

    // Moves the udpate panel (if the panel is actived on a unit and that unit moves)
    public void MoveInfoPanel(Unit unit) {
        if (unit.Equals(currentInfoUnit))
        {
            unitInfoPanel.transform.position = (Vector2)unit.transform.position + unitInfoPanelShift;
        }
    }

    // Deactivate info panel (when a unit dies)
    public void RemoveInfoPanel(Unit unit) {
        if (unit.Equals(currentInfoUnit))
        {
            unitInfoPanel.SetActive(false);
			currentInfoUnit = null;
        }
    }

    public void ResetTiles() {
        Tile[] tiles = FindObjectsOfType<Tile>();
        foreach (Tile tile in tiles)
        {
            tile.Reset();
        }
    }

    public void EndTurn() {
		source.Play();
        camAnim.SetTrigger("shake");

        // deselects the selected unit when the turn ends
        if (selectedUnit != null) {
            selectedUnit.ResetWeaponIcon();
            selectedUnit.isSelected = false;
            selectedUnit = null;
        }

        ResetTiles();

        Unit[] units = FindObjectsOfType<Unit>();
        foreach (Unit unit in units) {
            unit.hasAttacked = false;
            unit.hasMoved = false;
            unit.ResetWeaponIcon();
        }

        GetComponent<CharacterCreation>().CloseCharacterCreationMenus();
        createdUnit = null;

        if (playerTurn == 1) {
            if(!(gamePhase == GamePhase.DRAFT && player2Gold < lowerCost))
            {
                playerIcon.sprite = playerTwoIcon;
                playerTurn = 2;
            }
        } else if (playerTurn == 2) {
            if (!(gamePhase == GamePhase.DRAFT && player1Gold < lowerCost))
            {
                playerIcon.sprite = playerOneIcon;
                playerTurn = 1;
            }
        }
        if (gamePhase == GamePhase.DRAFT)
        {
            if (playerTurn == 2) randomAI.makeDraft(); //AI decisions
        }
        else if (gamePhase == GamePhase.BATTLE)
        {
            GetGoldIncome(playerTurn);
            if(playerTurn == 2) randomAI.startBattle(); //AI decisions
        }

    }

    void GetGoldIncome(int playerTurn) {
        foreach (Village village in FindObjectsOfType<Village>())
        {
            if (village.playerNumber == playerTurn)
            {
                if (playerTurn == 1)
                {
                    player1Gold += village.goldPerTurn;
                }
                else
                {
                    player2Gold += village.goldPerTurn;
                }
            }
        }
        UpdateGoldText();
    }

    public void UpdateGoldText()
    {
        player1GoldText.text = player1Gold.ToString();
        player2GoldText.text = player2Gold.ToString();
    }

    // Victory UI

    public void ShowVictoryPanel(int playerNumber) {

        if (playerNumber == 1)
        {
            blueVictory.SetActive(true);
        } else if (playerNumber == 2) {
            darkVictory.SetActive(true);
        }
    }

    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void RandomizeTrees()
    {
        Tile[] tiles = FindObjectsOfType<Tile>();
        int counter = 0;
        foreach (Tile tile in tiles)
        {
            //counter will mitigate the number of trees spawned
            counter++;
            //If it's not begin zone, you can't place a tree there
            if (tile.TreeZone && tile.isClear() && counter > 8)
            {
                //20% probability to place a tree on that tile
                if(Random.Range(0, 10) <= 2)
                {
                    counter = 0;
                    tile.SpawnTree();
                }
            }
        }
    }
}
