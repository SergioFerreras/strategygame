﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    [SerializeField] private BTree draftTree;
    [SerializeField] private BTree buyTree;
    GM gm;

    private void Start()
    {
        gm = FindObjectOfType<GM>();
    }

    public void makeDraft()
    {
        draftTree.EvaluateTree();
    }

    public void startBattle()
    {
        StartCoroutine("startBTreeMoveAttack");
    }

    private IEnumerator startBTreeMoveAttack()
    {
        Unit[] allUnits = FindObjectsOfType<Unit>();
        foreach (Unit unit in allUnits)
        {
            if (unit != null && unit.playerNumber == 2 && unit.GetComponent<Village>() == null && !unit.isTree)
            {
                while (!unit.UnitTree.EvaluateTree())
                {
                    yield return null;
                }
            }
        }
        startBTreeBuy();
    }

    private void startBTreeBuy()
    {
        buyTree.EvaluateTree();
        gm.EndTurn(); //End current AI turn
    }
}
