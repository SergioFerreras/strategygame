﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private SpriteRenderer rend;
    public Color highlightedColor;
    public Color creatableColor;

    public LayerMask obstacles;

    public bool isWalkable;
    public bool isCreatable;

    private GM gm;

    public float amount;
    private bool sizeIncrease;

	private AudioSource source;

    public bool TreeZone;
    public GameObject TreePrefab;

    public Node node;

    public int PlayerTile;
    

    private void Start()
    {
		source = GetComponent<AudioSource>();
        gm = FindObjectOfType<GM>();
        rend = GetComponent<SpriteRenderer>();

    }

    public bool isClear() // does this tile have an obstacle on it. Yes or No?
    {
        Collider2D col = Physics2D.OverlapCircle(transform.position, 0.2f, obstacles);
        if(col == null)
        {
            return true;
        }
        return false;
    }

    public void Highlight() {
		
        rend.color = highlightedColor;
        isWalkable = true;
    }

    public void Reset()
    {
        rend.color = Color.white;
        isWalkable = false;
        isCreatable = false;
    }

    public void SetCreatable() {
        rend.color = creatableColor;
        isCreatable = true;
    }

    private void OnMouseDown()
    {
        if(gm.playerTurn == 1 || gm.gamePhase == GamePhase.DRAFT) //ToDo: Take out the draft bool
        {
            if (isWalkable == true && gm.gamePhase == GamePhase.BATTLE)
            {
                MoveUnit();
            }
            else if (isCreatable == true && gm.createdUnit != null)
            {
                CreateUnit();
            }
            else if (isCreatable == true && gm.createdVillage != null)
            {
                CreateVillage();
            }
        }
    }

    public void MoveUnit()
    {
        gm.selectedUnit.Move(this);
    }

    public void CreateUnit()
    {
        Unit unit = Instantiate(gm.createdUnit, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
        unit.hasMoved = true;
        unit.hasAttacked = true;
        gm.ResetTiles();
        gm.createdUnit = null;
        if (gm.gamePhase == GamePhase.DRAFT)
            gm.EndTurn();
    }

    public void CreateVillage()
    {
        Instantiate(gm.createdVillage, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
        gm.ResetTiles();
        gm.createdVillage = null;
        if (gm.gamePhase == GamePhase.DRAFT)
            gm.EndTurn();
    }

    private void OnMouseEnter()
    {
        if (isClear() == true) {
			source.Play();
			sizeIncrease = true;
            transform.localScale += new Vector3(amount, amount, amount);
        }
        
    }

    private void OnMouseExit()
    {
        if (isClear() == true)
        {
            sizeIncrease = false;
            transform.localScale -= new Vector3(amount, amount, amount);
        }

        if (isClear() == false && sizeIncrease == true) {
            sizeIncrease = false;
            transform.localScale -= new Vector3(amount, amount, amount);
        }
    }

    public void SpawnTree()
    {
        GameObject tree = Instantiate(TreePrefab);
        tree.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,0);
    }
}
