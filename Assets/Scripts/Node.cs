﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{

    public bool Walkable { get; set; } //Can you walk trough the node or is it an obstacle
    public Tile Tile { get; set; } //tile that the node represents in the graph

    public Node LeftNode
    {
        get { return Neighbours[0]; }
        set { Neighbours[0] = value; }
    }
    public Node RightNode
    {
        get { return Neighbours[1]; }
        set { Neighbours[1] = value; }
    }
    public Node TopNode
    {
        get { return Neighbours[2]; }
        set { Neighbours[2] = value; }
    }
    public Node BottomNode
    {
        get { return Neighbours[3]; }
        set { Neighbours[3] = value; }
    }


    public Node[] Neighbours { get; set; }

    public int index;

    public Node(bool walkable,Tile tile, Node leftNode = null, Node rightNode = null, Node topNode = null, Node bottomNode = null)
    {
        Walkable = walkable;
        Tile = tile;
        Neighbours = new Node[4]; //may contain null entries, take into account when searching the graph
        LeftNode = leftNode;
        RightNode = rightNode;
        TopNode = topNode;
        BottomNode = bottomNode;
    }
    public float TotalInfluence=0;

    public float ValueCanBeHit=0; //+1 if it can be reached by an enemy attack, zero if it can't (MAX 1 per enemy)
    public float ValueEnemyKingClose=0; //bigger when enemy king is closer (MAX 1) -ignores obstacles-
    public float ValueAlliesClose=0; //bigger when allies are closer (MAX 1 per ally) -ignores obstacles-
    public float ValueKillableEnemies=0; //1 if you can kill enemy in that node (MAX 1 per enemy)
    public float ValueHitableEnemies=0; //1 if you can kill enemy in that node (MAX 1 per enemy)
    public float ValueEnemiesClose=0; //bigger when enemies are closer (MAX 1 per enemy) -ignores obstacles-
    public float ValueAllyKingClose=0; //bigger when ally king is closer (MAX 1) -ignores obstacles-
    public float ValueCanHitKing=0; //1 if king can be hit from there 0 if not (1 or 0)
    public float ValueCanKillKing=0; //1 if king can be killed from there 0 if not (1 or 0)
}