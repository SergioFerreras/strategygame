﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum UnitType
{
    KING, KNIGHT, ARCHER, BAT, VILLAGE, TREE
}

public class Unit : MonoBehaviour
{
    public UnitType unitType;

    public bool isSelected;
    public bool hasMoved;

    public int tileSpeed;
    public float moveSpeed;

    private GM gm;

    public int attackRadius;
    public bool hasAttacked;
    public List<Unit> enemiesInRange = new List<Unit>();

    public int playerNumber;

    public GameObject weaponIcon;

    // Attack Stats
    public int health;
    public int attackDamage;
    public int defenseDamage;
    public int armor;

    public DamageIcon damageIcon;

    public int cost;

	public GameObject deathEffect;

	private Animator camAnim;

    public bool isKing;
    public bool isFlying;
    public bool isTree; //bool for trees (both players can attack them)

	private AudioSource source;

    public Text displayedText;

    private InfluenceMapGraph graph;

    public LayerMask tilesLayer;

    private List<Tile> walkableTiles = new List<Tile>();

    public BTree UnitTree;

    private void Start()
    {
        graph = FindObjectOfType<InfluenceMapGraph>();
		source = GetComponent<AudioSource>();
		camAnim = Camera.main.GetComponent<Animator>();
        gm = FindObjectOfType<GM>();
        UpdateHealthDisplay();
    }

    private void UpdateHealthDisplay ()
    {
        if (isKing)
        {
            displayedText.text = health.ToString();
        }
    }

    private void OnMouseDown() // select character or deselect if already selected
    {

        //graph.updateInfluenceValues(this); //FOR TESTING PURPOSES

        ResetWeaponIcon();

        if (gm.playerTurn == 1)
        {

            if (isSelected == true)
            {

                isSelected = false;
                gm.selectedUnit = null;
                gm.ResetTiles();

            }
            else
            {
                if (playerNumber == gm.playerTurn && !isTree)
                { // select unit only if it's his turn
                    if (gm.selectedUnit != null)
                    { // deselect the unit that is currently selected, so there's only one isSelected unit at a time
                        gm.selectedUnit.isSelected = false;
                    }
                    gm.ResetTiles();

                    gm.selectedUnit = this;

                    isSelected = true;
                    if (source != null)
                    {
                        source.Play();
                    }

                    GetWalkableTiles();
                    if (gm.gamePhase == GamePhase.BATTLE)
                        GetEnemies();
                }

            }

            Collider2D col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.15f);
            if (col != null)
            {
                Unit unit = col.GetComponent<Unit>(); // double check that what we clicked on is a unit
                if (unit != null && gm.selectedUnit != null)
                {
                    if (gm.selectedUnit.enemiesInRange.Contains(unit) && !gm.selectedUnit.hasAttacked)
                    { // does the currently selected unit have in his list the enemy we just clicked on
                        gm.selectedUnit.Attack(unit);

                    }
                }
            }
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            gm.UpdateInfoPanel(this);
        }
    }



    public List<Tile> GetWalkableTiles() { // Looks for the tiles the unit can walk on
        walkableTiles.Clear();
        if (hasMoved == true) {
            return walkableTiles;
        }
        if (isFlying)
            graph.RunOnEachTileBFS(highlightTile, getCurrentTile(), tileSpeed,true);
        else
            graph.RunOnEachTileBFS(highlightTile, getCurrentTile(), tileSpeed, false);
        walkableTiles.Add(getCurrentTile());
        return walkableTiles;
    }

    void highlightTile(Tile tile)
    {
        walkableTiles.Add(tile);
        tile.Highlight(); 
    }

    public Tile getCurrentTile()
    {
        Collider2D col = Physics2D.OverlapCircle(transform.position, 0.2f, tilesLayer);

        Tile t = null;
        if (col != null)
            t = col.gameObject.GetComponent<Tile>();
        if (t==null)
            Debug.LogError("Unit unable to find current tile");

        return t;
    }

    void GetEnemies() { 
    
        enemiesInRange.Clear();
        Unit[] enemies = FindObjectsOfType<Unit>();
        foreach (Unit enemy in enemies)
        {
            if (Mathf.Abs(transform.position.x - enemy.transform.position.x) + Mathf.Abs(transform.position.y - enemy.transform.position.y) <= attackRadius) // check is the enemy is near enough to attack
            {
                if ((enemy.isTree || enemy.playerNumber != gm.playerTurn) && !hasAttacked) { // make sure you don't attack your allies
                    enemiesInRange.Add(enemy);
                    enemy.weaponIcon.SetActive(true);
                }

            }
        }
    }

    public void Move(Tile movePos)
    {
        if(gm.gamePhase == GamePhase.BATTLE)
        {
            GetWalkableTiles();
            gm.ResetTiles();
            StartCoroutine(StartMovement(graph.getPathTo(movePos)));
        }
    }

    public void Attack(Unit enemy) {
        hasAttacked = true;

        int enemyDamege = attackDamage - enemy.armor;
        int unitDamage = enemy.defenseDamage - armor;

        if (enemyDamege >= 1)
        {
            enemy.health -= enemyDamege;
            enemy.UpdateHealthDisplay();
            DamageIcon d = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            d.Setup(enemyDamege);
        }

        if (transform.tag == "Archer" && enemy.tag != "Archer")
        {
            if (Mathf.Abs(transform.position.x - enemy.transform.position.x) + Mathf.Abs(transform.position.y - enemy.transform.position.y) <= 1) // check is the enemy is near enough to attack
            {
                if (unitDamage >= 1)
                {
                    health -= unitDamage;
                    UpdateHealthDisplay();
                    DamageIcon d = Instantiate(damageIcon, transform.position, Quaternion.identity);
                    d.Setup(unitDamage);
                }
            }
        } else {
            if (unitDamage >= 1)
            {
                health -= unitDamage;
                UpdateHealthDisplay();
                DamageIcon d = Instantiate(damageIcon, transform.position, Quaternion.identity);
                d.Setup(unitDamage);
            }
        }

        if (enemy.health <= 0)
        {
         
            if (deathEffect != null){
				Instantiate(deathEffect, enemy.transform.position, Quaternion.identity);
				camAnim.SetTrigger("shake");
			}

            if (enemy.isKing)
            {
                gm.ShowVictoryPanel(enemy.playerNumber);
            }

            GetWalkableTiles(); // check for new walkable tiles (if enemy has died we can now walk on his tile)
            gm.RemoveInfoPanel(enemy);
            Destroy(enemy.gameObject);
        }

        if (health <= 0)
        {

            if (deathEffect != null)
			{
				Instantiate(deathEffect, enemy.transform.position, Quaternion.identity);
				camAnim.SetTrigger("shake");
			}

			if (isKing)
            {
                gm.ShowVictoryPanel(playerNumber);
            }

            gm.ResetTiles(); // reset tiles when we die
            gm.RemoveInfoPanel(this);
            Destroy(gameObject);
        }

        gm.UpdateInfoStats();
  

    }

    public void ResetWeaponIcon() {
        Unit[] enemies = FindObjectsOfType<Unit>();
        foreach (Unit enemy in enemies)
        {
            enemy.weaponIcon.SetActive(false);
        }
    }

    IEnumerator StartMovement(Stack<Vector3> movePath) { // Moves the character to his new position.


        while (movePath.Count > 0)
        {
            while (transform.position.x != movePath.Peek().x)
            { // first aligns him with the new tile's x pos
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(movePath.Peek().x, transform.position.y), moveSpeed * Time.deltaTime);
                yield return null;
            }
            while (transform.position.y != movePath.Peek().y) // then y
            {
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, movePath.Peek().y), moveSpeed * Time.deltaTime);
                yield return null;
            }
            movePath.Pop();
        }
        hasMoved = true;
        ResetWeaponIcon();
        GetEnemies();
        gm.MoveInfoPanel(this);
    }

    public Node GetNode()
    {
        return getCurrentTile().node;
    }
}
